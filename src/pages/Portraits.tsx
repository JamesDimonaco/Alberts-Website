import React from "react";
import "./Home.css";
import Header from "../components/Header";
import { IonPage, IonContent, IonHeader, IonImg } from "@ionic/react";

const image: string = "cdn.discordapp.com/attachments/533906819324837944/7206";

const Portraits: React.FC = () => {
  return (
    <IonPage>
      <IonContent>
        <IonHeader>
          <div style={{}}>
            <Header theme="" />
          </div>
        </IonHeader>

        <div className="">
          <IonImg
            id="one"
            src={image + "39819151048834/james2_done.jpg"}
            alt=""
          />
        </div>
        <div className="flex">
          <div className="" id="two">
            <IonImg src={image + "39799542677584/jamesdone1.jpg"} alt="" />
          </div>

          <div id="three">
            <IonImg src={image + "40374858842134/james4.JPG"} alt="" />
          </div>
        </div>
        <div id="four">
          <IonImg src={image + "39930300104715/jam88.jpg"} alt="" />
        </div>
        <div className="flex">
          <div id="five">
            <IonImg
              src="https://storage.googleapis.com/web-images-for-bert/Edited%20all/jamie%20Portraits%20pic%205"
              alt=""
            />
          </div>
          <div id="six">
            <IonImg
              src="https://storage.googleapis.com/web-images-for-bert/Edited%20all/Jamie%20Portraits%20pic%206.JPG"
              alt=""
            />
          </div>
        </div>
        <div className="flex">
          <div id="six">
            <IonImg
              src={image + "39880945729706/jamie_instasend_.jpg"}
              alt=""
            />
          </div>
          <div id="five">
            <img src={image + "39927573807184/jam3.jpg"} alt="" />
          </div>
        </div>

        <div id="seven">
          <IonImg
            src="https://cdn.discordapp.com/attachments/533906819324837944/723537677655474256/unknown.png"
            alt=""
          />
        </div>

        <div className="flexpic">
          <div id="eight">
            <IonImg src={image + "41274281197648/alina4.jpg"} alt="" />
          </div>
          <div id="nine">
            <IonImg
              src="https://cdn.discordapp.com/attachments/533906819324837944/723534775994024008/unknown.png"
              alt=""
            />
          </div>
        </div>
        <div id="ten">
          <IonImg
            src="https://cdn.discordapp.com/attachments/533906819324837944/723535976714076201/unknown.png"
            alt=""
          />
        </div>
        <div id="tennn">
          <IonImg
            src="https://cdn.discordapp.com/attachments/533906819324837944/723536901675417611/unknown.png"
            alt=""
          />
        </div>
        <div id="tenn">
          <IonImg
            src="https://cdn.discordapp.com/attachments/533906819324837944/723537013550350456/unknown.png"
            alt=""
          />
        </div>
        <div id="tennn">
          <IonImg
            src="https://cdn.discordapp.com/attachments/533906819324837944/723537143321853952/unknown.png"
            alt=""
          />
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Portraits;
