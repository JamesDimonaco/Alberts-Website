import React from "react";
import "./General.scss";
import Header from "../components/Header";
import { IonPage, IonContent, IonImg } from "@ionic/react";

export const General: React.FC = () => {
  return (
    <IonPage>
      <IonContent className="portraits">
        <Header theme="portraitsH" />

        <IonImg
          src="https://cdn.discordapp.com/attachments/771023334834044969/771023409072046130/General.png"
          alt=""
        />
      </IonContent>
    </IonPage>
  );
};
