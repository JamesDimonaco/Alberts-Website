import React from "react";
import "./imagePlacement.css";
import Header from "../components/Header";
import { IonPage, IonContent, IonImg } from "@ionic/react";

// const image: string = 'https://cdn.discordapp.com/attachments/'
const image: string =
  "https://cdn.discordapp.com/attachments/734782759339556915/7347";

export const Travel: React.FC = () => {
  return (
    <IonPage>
      <IonContent>
        <Header theme="Travel" />

        <IonImg
          id=""
          src="https://cdn.discordapp.com/attachments/533906819324837944/767492714408312902/TRAVEl.png"
          alt=""
        />
        <IonImg
          id="two"
          src="https://cdn.discordapp.com/attachments/767445852863725668/768133813451620362/moroc9.jpg"
          alt=""
        />
        <IonImg
          id="three"
          src="https://cdn.discordapp.com/attachments/767445852863725668/768133861519130654/moroc11.jpg"
          alt=""
        />
        <IonImg
          id="nine"
          src="https://cdn.discordapp.com/attachments/767445852863725668/767446107746730054/guard1-2.png"
          alt=""
        />
        <IonImg
          id="five"
          src="https://cdn.discordapp.com/attachments/767445852863725668/768133857115242506/moro8.jpg"
          alt=""
        />
        <IonImg
          id="six"
          src="https://cdn.discordapp.com/attachments/767445852863725668/768133818673528832/moroc8.jpg"
          alt=""
        />
        <IonImg
          id="seven"
          src="https://cdn.discordapp.com/attachments/767445852863725668/768133859593420810/morrocco4.jpg"
          alt=""
        />
        <div className="flex">
          <IonImg
            id="eight"
            src="https://cdn.discordapp.com/attachments/767445852863725668/768133870284701717/train_pgtp_1.png"
            alt=""
          />
          <IonImg
            id="nine"
            src="https://cdn.discordapp.com/attachments/767445852863725668/768133857273708584/girl-23_with_moon_1.png"
            alt=""
          />
        </div>
        <div className="flex">
          <IonImg
            id=""
            src="https://cdn.discordapp.com/attachments/767445852863725668/767446065938825276/jamienotonphone.jpg"
            alt=""
          />
          <IonImg
            id=""
            src="https://cdn.discordapp.com/attachments/767445852863725668/767446061253132338/over_the_edge.png"
            alt=""
          />
        </div>
        <div className="flex">
          <IonImg
            id=""
            src="https://cdn.discordapp.com/attachments/767445852863725668/767446058103865344/malta2.png"
            alt=""
          />
          <IonImg
            id=""
            src="https://cdn.discordapp.com/attachments/767445852863725668/767446093742342184/malta3.png"
            alt=""
          />
        </div>
        <IonImg id="" src={image + "83034313801768/15-min.png"} alt="" />
        <IonImg id="" src={image + "83037761519696/16-min.png"} alt="" />
        <IonImg id="" src={image + "83041477804092/17-min.png"} alt="" />
        <IonImg id="" src={image + "83067113259028/18-min.png"} alt="" />
        <IonImg id="" src={image + "83069579640832/19-min.png"} alt="" />
        <IonImg id="" src={image + "83071680724992/20-min.png"} alt="" />
        <IonImg id="" src={image + "83074189049856/21-min.png"} alt="" />
        <IonImg id="" src={image + "3077007491102/22-min.png"} alt="" />
      </IonContent>
    </IonPage>
  );
};
