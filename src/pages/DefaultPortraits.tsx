import React from "react";
import "./DefaultPortraits.scss";
import Header from "../components/Header";
import { IonPage, IonContent, IonImg } from "@ionic/react";

const DefaultPortraits: React.FC = () => {
  return (
    <IonPage>
      <IonContent className="portraits">
        <Header theme="portraitsH" />

        <IonImg
          id="one"
          src="https://cdn.discordapp.com/attachments/768110813885628416/768132737227423774/PORTRAITS.png"
          alt=""
        />

        <div className="flex">
          <div className="" id="two">
            <IonImg
              id="space"
              src="https://cdn.discordapp.com/attachments/767418045093642242/767443160208244746/James_done.png"
              alt=""
            />
          </div>

          <div id="three">
            <IonImg
              id="space"
              src="https://cdn.discordapp.com/attachments/767418045093642242/767443213975420978/James_baw.jpg"
              alt=""
            />
          </div>
        </div>
        <div className="flex">
          <div className="" id="two">
            <IonImg
              src="https://cdn.discordapp.com/attachments/727496094019485797/727497517859602522/2-min.jpg"
              alt=""
            />
          </div>

          <IonImg
            id="two"
            src="https://cdn.discordapp.com/attachments/727496094019485797/727497526844063764/3-min.jpeg"
            alt=""
          />
        </div>

        <IonImg
          id="two"
          src="https://cdn.discordapp.com/attachments/768110813885628416/768110846818648064/jamie_baw.png"
          alt=""
        />

        <div className="flex">
          <IonImg
            id="two"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768113819595178064/jam88.jpg"
            alt=""
          />

          <IonImg
            id="jamieLittleHead"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768112209741086730/Portraits.png"
            alt=""
          />
        </div>
        <div className="flex" id="two">
          <IonImg
            src="https://cdn.discordapp.com/attachments/768110813885628416/768113895696891964/Jamie99.jpg"
            alt=""
          />

          <IonImg
            id="three"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768113895696891964/Jamie99.jpg"
            alt=""
          />
        </div>
        <div className="flex">
          <div className="" id="two">
            <IonImg
              src="https://cdn.discordapp.com/attachments/768110813885628416/768113896333901824/jamierain2.jpg"
              alt=""
            />
          </div>

          <IonImg
            id="two"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768113892810817596/jam3.jpg"
            alt=""
          />
        </div>

        <IonImg
          src="https://cdn.discordapp.com/attachments/768110813885628416/768120136829632552/kikaheadshotdone1.jpg"
          alt=""
        />
        <div className="flex">
          <IonImg
            id="kikaL"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768119985926176778/kikainstagram1.jpg"
            alt=""
          />

          <IonImg
            id="kikaR"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768119981882736650/kikainstagram2.jpg"
            alt=""
          />
        </div>
        <IonImg
          src="https://cdn.discordapp.com/attachments/768110813885628416/768120528766500954/alina2.jpg"
          alt=""
        />

        <div className="flex">
          <IonImg
            id="alL"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768120617777627156/alina4.png"
            alt=""
          />

          <IonImg
            id="alR"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768120666821099550/alina3.jpg"
            alt=""
          />
        </div>
        <IonImg
          src="https://cdn.discordapp.com/attachments/768110813885628416/768121878371369010/blue_hair_top_down1.jpg"
          alt=""
        />

        <div className="flex">
          <IonImg
            id="alL"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768121797434146826/harleighinsta1.jpg"
            alt=""
          />

          <IonImg
            id="alR"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768121794367979560/harleighinsta2.jpg"
            alt=""
          />
        </div>
        <IonImg
          src="https://cdn.discordapp.com/attachments/768110813885628416/768122487389028362/sara_mask_1_.jpg"
          alt=""
        />

        <div className="flex">
          <IonImg
            id="alL"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768122554472595477/sara_si1.jpg"
            alt=""
          />

          <IonImg
            id="alR"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768122556371697694/sara_purplebackgroundinditbl.png"
            alt=""
          />
        </div>
        <IonImg
          src="https://cdn.discordapp.com/attachments/768110813885628416/768123400232370206/rissa3.png"
          alt=""
        />

        <div className="flex">
          <IonImg
            id="alL"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768123316560068678/rissa4.png"
            alt=""
          />

          <IonImg
            id="kikaR"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768123296519684096/Rissa7.png"
            alt=""
          />
        </div>
        <div className="flex">
          <IonImg
            id="alL"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768123319122919464/Rissa6.jpg"
            alt=""
          />

          <IonImg
            id="kikaR"
            src="https://cdn.discordapp.com/attachments/768110813885628416/768123288991563776/rissa6.png"
            alt=""
          />
        </div>
        <IonImg
          id="space"
          src="https://cdn.discordapp.com/attachments/768110813885628416/768125171529154620/ukraine4.jpg"
          alt=""
        />
        <IonImg
          id="space"
          src="https://cdn.discordapp.com/attachments/768110813885628416/768125165137166416/dad1.jpg"
          alt=""
        />
        <IonImg
          id="space"
          src="https://cdn.discordapp.com/attachments/768110813885628416/768125168627089438/Thai1.jpg"
          alt=""
        />
        <IonImg
          id="space"
          src="https://cdn.discordapp.com/attachments/768110813885628416/768125167045967942/Maria_redline_3-1.jpg"
          alt=""
        />
      </IonContent>
    </IonPage>
  );
};

export default DefaultPortraits;
