import { IonContent, IonPage, IonImg } from "@ionic/react";
import React from "react";
import "./Home.css";
import Header from "../components/Header";

const Home: React.FC = () => {
  return (
    <IonPage>
      <IonContent scrollEvents={true} className="homeBackground" scrollY={true}>
        <Header theme="home" />

        <IonImg
          src="https://cdn.discordapp.com/attachments/533906819324837944/767493070878539776/Untitled-2.png"
          alt="Albert"
        />
      </IonContent>
    </IonPage>
  );
};

export default Home;
