import React from "react";
import Header from "../components/Header";
import {
  IonPage,
  IonContent,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonImg,
} from "@ionic/react";
import "./Home.css";

export const Works: React.FC = () => {
  const frontImage: string[] = [
    "https://cdn.discordapp.com/attachments/766829810714148915/766829885820895293/protest1.jpg",
    "https://cdn.discordapp.com/attachments/767418045093642242/767418565418287134/Portraits.png",
    "https://cdn.discordapp.com/attachments/767418045093642242/767418132263469056/Travel.png",
    "https://cdn.discordapp.com/attachments/767418045093642242/767418136789385236/Fine_art.jpg",
    "https://cdn.discordapp.com/attachments/767418045093642242/767418135347200000/Architecture.jpg",
    "https://cdn.discordapp.com/attachments/767418045093642242/767419919624568868/Brand.jpg",
  ];

  return (
    <IonPage>
      <IonContent className="Background">
        <Header theme="works" />

        <div className="flex" id="wrap">
          <IonCard
            className="wide"
            id="topRow"
            button={true}
            routerLink="/general"
          >
            <IonImg id="bigCard" src={frontImage[0]} alt="" />
            <IonCardHeader>
              <IonCardTitle className="flexIiem">GENRERAL</IonCardTitle>
            </IonCardHeader>
          </IonCard>

          <IonCard
            id="topRow"
            className="cards"
            button={true}
            routerLink="/defaultportraits"
          >
            <IonImg src={frontImage[1]} alt="" />
            <IonCardHeader>
              <IonCardTitle className="flexIiem">PORTRAITS</IonCardTitle>
            </IonCardHeader>
          </IonCard>

          <IonCard
            className="wide"
            id="topRow"
            button={true}
            routerLink="/travel"
          >
            <IonImg id="bigCard" src={frontImage[2]} alt="" />
            <IonCardHeader>
              <IonCardTitle className="flexIiem">TRAVEL</IonCardTitle>
            </IonCardHeader>
          </IonCard>
        </div>
        <div className="flex" id="wrap">
          <IonCard className="cards" button={true} routerLink="/fineart">
            <IonImg id="bigCard" src={frontImage[3]} alt="" />
            <IonCardHeader>
              <IonCardTitle className="flexIiem">FINE ART</IonCardTitle>
            </IonCardHeader>
          </IonCard>

          <IonCard className="cards" button={true} routerLink="/arcitecture">
            <IonImg id="bigCard" src={frontImage[4]} alt="" />
            <IonCardHeader>
              <IonCardTitle className="flexIiem">ARCHITECTURE</IonCardTitle>
            </IonCardHeader>
          </IonCard>
          <IonCard className="cards" button={true} routerLink="/brand">
            <IonImg id="bigCard" src={frontImage[5]} alt="" />
            <IonCardHeader>
              <IonCardTitle className="flexIiem">BRAND</IonCardTitle>
            </IonCardHeader>
          </IonCard>
        </div>
      </IonContent>
    </IonPage>
  );
};
