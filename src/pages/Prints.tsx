import React from "react";
import "./imagePlacement.css";
import Header from "../components/Header";
import { IonPage, IonContent } from "@ionic/react";

export const Prints: React.FC = () => {
  return (
    <IonPage>
      <IonContent className="Background" scrollY={false}>
        <Header theme="works" />

        <h1 className="middle">
          <strong>COMING SOON</strong>
        </h1>
      </IonContent>
    </IonPage>
  );
};
