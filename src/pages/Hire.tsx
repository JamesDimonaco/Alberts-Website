import React, { useState } from "react";
import "./Home.css";
import Header from "../components/Header";
import {
  IonPage,
  IonContent,
  IonList,
  IonItem,
  IonInput,
  IonLabel,
  IonItemDivider,
  IonCheckbox,
  IonTextarea,
  IonButton,
  IonDatetime,
} from "@ionic/react";

export const Hire: React.FC = () => {
  let today: string = "2020-10-27";

  const [hiddenList, setHiddenList] = useState(false);
  const [showMsg, setShowMsg] = useState(true);
  const [locationOfPhotographer, setLocationOfPhotographer] = useState<
    string
  >();
  const [otherInformation, setOtherInformation] = useState<string>();
  const [selectedDate, setSelectedDate] = useState<string>();
  const [firstName, setFirstName] = useState<string>();
  const [lastName, setlastName] = useState<string>();
  const [email, setEmail] = useState<string>();
  const [portraitChecked, setPortraitChecked] = useState(false);
  const [companyPortraitsChecked, setCompanyPortraitsChecked] = useState(false);
  const [brandChecked, setBrandChecked] = useState(false);
  const [architecturalChecked, setArchitecturalChecked] = useState(false);
  const [eventChecked, setEventChecked] = useState(false);
  const [otherChecked, setOtherChecked] = useState(false);
  const [yesCTSChecked, setYesCTSChecked] = useState(false);
  const [yesBPSTMChecked, setYesBPSTMChecked] = useState(false);
  const [noChecked, setNoChecked] = useState(false);
  const [whereNotSureYetChecked, setWhereNotSureYetChecked] = useState(false);
  const [oneChecked, set100Checked] = useState(false);
  const [twoChecked, set250Checked] = useState(false);
  const [threeChecked, set500Checked] = useState(false);
  const [fourChecked, set1000Checked] = useState(false);
  const [budgetNotSureYetChecked, setBudgetNotSureYetChecked] = useState(false);

  // const [Checked, setChecked] = useState(false);

  function hideList() {
    setHiddenList(true);
    setShowMsg(false);
    console.log(
      ` First name:${firstName}
         Last name:${lastName} 
             Email:${email} 

                Portrait:${portraitChecked} 
       Company portraits:${companyPortraitsChecked} 
                   Brand:${brandChecked} 
           Architectural:${architecturalChecked} 
                   Event:${eventChecked} 
                   Other:${otherChecked}

       Do you need a studio?
                    Yes (Come to Studio):${yesCTSChecked}
       Yes (Bring portable studio to me):${yesBPSTMChecked}
                                      no:${noChecked} 

       Date you have selected: ${selectedDate}

       Where do you need the photographer?
          ${locationOfPhotographer}

       What's your budget?
           £100 - £250:${oneChecked}
           £250 - £500:${twoChecked}
          £500 - £1000:${threeChecked}
                £1000+:${fourChecked}
          Not sure yet:${budgetNotSureYetChecked}

        Any other info:${otherInformation}
       `
    );
  }

  return (
    <IonPage>
      <IonContent className="Background">
        <Header theme="works" />
        <IonList className="msg" hidden={showMsg}>
          <IonLabel color="success">
            Thank you, Albert will be in contact with you soon.
          </IonLabel>
        </IonList>
        <IonList hidden={hiddenList} className="List">
          <IonItem>
            <IonLabel id={firstName} position="floating">
              First Name
            </IonLabel>
            <IonInput
              required={true}
              autofocus={true}
              value={firstName}
              onIonChange={(e) => setFirstName(e.detail.value!)}
              clearInput
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel id="surname" position="floating">
              Surname
            </IonLabel>
            <IonInput
              required={true}
              autocapitalize="on"
              value={lastName}
              onIonChange={(e) => setlastName(e.detail.value!)}
              clearInput
            ></IonInput>
          </IonItem>

          <IonItem>
            <IonLabel position="floating">Email Address</IonLabel>

            <IonInput
              required={true}
              autocomplete="on"
              type="email"
              value={email}
              onIonChange={(e) => setEmail(e.detail.value!)}
              clearInput
            ></IonInput>
          </IonItem>

          {/*-- Item Dividers in a List --*/}
          <IonList>
            <IonItemDivider color="warning">
              <IonLabel>What type of photography do you need?</IonLabel>
            </IonItemDivider>

            <IonItem>
              <IonLabel>Portrait</IonLabel>
              <IonCheckbox
                checked={portraitChecked}
                onIonChange={(e) => setPortraitChecked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>Company Portraits </IonLabel>
              <IonCheckbox
                checked={companyPortraitsChecked}
                onIonChange={(e) =>
                  setCompanyPortraitsChecked(e.detail.checked)
                }
              />
            </IonItem>
            <IonItem>
              <IonLabel>Brand </IonLabel>
              <IonCheckbox
                checked={brandChecked}
                onIonChange={(e) => setBrandChecked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>Architectural </IonLabel>
              <IonCheckbox
                checked={architecturalChecked}
                onIonChange={(e) => setArchitecturalChecked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>Event</IonLabel>
              <IonCheckbox
                checked={eventChecked}
                onIonChange={(e) => setEventChecked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>Other</IonLabel>
              <IonCheckbox
                checked={otherChecked}
                onIonChange={(e) => setOtherChecked(e.detail.checked)}
              />
            </IonItem>

            <IonItemDivider color="warning">
              <IonLabel>Do you need a studio? </IonLabel>
            </IonItemDivider>

            <IonItem>
              <IonLabel>Yes (Come to Studio) </IonLabel>
              <IonCheckbox
                checked={yesCTSChecked}
                onIonChange={(e) => setYesCTSChecked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>Yes (Bring portable studio to me)</IonLabel>
              <IonCheckbox
                checked={yesBPSTMChecked}
                onIonChange={(e) => setYesBPSTMChecked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>No </IonLabel>
              <IonCheckbox
                checked={noChecked}
                onIonChange={(e) => setNoChecked(e.detail.checked)}
              />
            </IonItem>
            <IonItemDivider color="warning">
              <IonLabel>What date is best for you? </IonLabel>
            </IonItemDivider>
            <IonItem>
              <IonLabel position="floating">Select date here</IonLabel>
              <IonDatetime
                displayFormat="DD/MMMM/YYYY"
                min={today}
                value={selectedDate}
                onIonChange={(e) => setSelectedDate(e.detail.value!)}
              ></IonDatetime>
            </IonItem>

            {/* figure out calinder solution */}
            <IonItemDivider color="warning">
              <IonLabel>Where do you need the photographer? </IonLabel>
            </IonItemDivider>
            <IonItem>
              <IonLabel position="floating">
                add all relevent information here
              </IonLabel>

              <IonTextarea
                value={locationOfPhotographer}
                onIonChange={(e) => setLocationOfPhotographer(e.detail.value!)}
              ></IonTextarea>
            </IonItem>

            <IonItem>
              <IonLabel>Not sure yet </IonLabel>
              <IonCheckbox
                checked={whereNotSureYetChecked}
                onIonChange={(e) => setWhereNotSureYetChecked(e.detail.checked)}
              />
            </IonItem>
            <IonItemDivider color="warning">
              <IonLabel>What's your budget? </IonLabel>
            </IonItemDivider>
            <IonItem>
              <IonLabel>£100 - £250 </IonLabel>
              <IonCheckbox
                checked={oneChecked}
                onIonChange={(e) => set100Checked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>£250 - £500 </IonLabel>
              <IonCheckbox
                checked={twoChecked}
                onIonChange={(e) => set250Checked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>£500 - £1000 </IonLabel>
              <IonCheckbox
                checked={threeChecked}
                onIonChange={(e) => set500Checked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>£1000+ </IonLabel>
              <IonCheckbox
                checked={fourChecked}
                onIonChange={(e) => set1000Checked(e.detail.checked)}
              />
            </IonItem>
            <IonItem>
              <IonLabel>Not sure yet </IonLabel>
              <IonCheckbox
                checked={budgetNotSureYetChecked}
                onIonChange={(e) =>
                  setBudgetNotSureYetChecked(e.detail.checked)
                }
              />
            </IonItem>
            <IonItemDivider color="warning">
              <IonLabel>Any other information to give? </IonLabel>
            </IonItemDivider>
            <IonItem>
              <IonTextarea
                placeholder="Type here"
                value={otherInformation}
                onIonChange={(e) => setOtherInformation(e.detail.value!)}
              ></IonTextarea>
            </IonItem>
          </IonList>
          <IonButton
            onClick={(e) => {
              hideList();
            }}
            expand="full"
            type="submit"
          >
            SUBMIT
          </IonButton>
        </IonList>
      </IonContent>
    </IonPage>
  );
};
